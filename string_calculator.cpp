#include <cstdlib>
#include <iostream>

using namespace std;

int add(string numbers)
{
    // local variables
    string numTemp = "0";
    string exception = "negatives not allowed:";
    char delimiter = '\0'; // null delimiter
    int sum = 0;
    int i = 0;
    bool isChanged = numbers.length() >= 4 && numbers[0] == '/' && numbers[1] == '/'; // checks if there's a custom delimiter
    bool isNegative = false;

    // check if it's empty
    if (numbers == "")
    {
        return 0;
    }

    // if it is not empty

    // checks if the delimiter has been changed
    if (isChanged)
    {
        delimiter = numbers[2]; // sets the delimiter accordingly
        i = 4;                  // sets the counter to the second line
    }

    // running through the string
    while (i < numbers.length())
    {
        // for each character

        // checks if there's a delimiter
        if (numbers[i] != ',' && numbers[i] != '\n' && numbers[i] != delimiter)
        {

            // checks if negative
            if (numbers[i] == '-')
            {
                isNegative = true;
                i++;

                // composes the exception message
                exception += " -";
                while (i < numbers.length() && numbers[i] != ',' && numbers[i] != '\n' && numbers[i] != delimiter)
                {
                    exception += numbers[i];
                    i++;
                }
            }
            else
            {
                numTemp += numbers[i];
            }
        }
        else
        {
            // checks if it the number is big and eventually sums it
            if (stoi(numTemp) <= 1000)
            {
                sum += stoi(numTemp);
            }
            numTemp = "0";
        }
        i++;
    }

    // runs the check on the last number
    if (stoi(numTemp) <= 1000)
    {
        sum += stoi(numTemp);
    }

    if (isNegative)
    {
        throw invalid_argument(exception);
    }

    return sum;
}

/*
 *
 */
int main(int argc, char **argv)
{
    cout << add("//U\n1000U4U2,5");
    return 0;
}
